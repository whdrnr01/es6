/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = {
	get: function get(key) {
		var value = localStorage.getItem(key);

		return new Promise(function (resolve, reject) {
			if (value === 'null') {
				reject('GET ERROR');
			}

			resolve(value);
		});
	},
	put: function put(key, value) {
		return new Promise(function (resolve, reject) {
			localStorage.setItem(key, value);

			resolve(value);
		});
	}
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _storage = __webpack_require__(0);

var _storage2 = _interopRequireDefault(_storage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_storage2.default.get('name').then(function (value) {
	console.log(value);
});

/*
// Promise Basic
let api = {
	register () {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				if (false) {
					reject({'error': 'Failed'})
				}
				resolve({'name': 'cookie'})
			}, 2000)
		})
	}
}

let component = {
	register () {
		api.register().then((response) => {
			console.log(response)
		}).catch(() => {
			console.log('Registration Failed')
		})
	}
}

component.register()
*/

/*
let p = new Promise((resolve, reject) => {
	var key = 'abc'
	var value = null

	if (value) {
		resolve(value)
	}

	reject(`No key found ${key}`)
})

p.then((value) => {
	console.log(value)
})

p.catch((value) => {
	console.log(value)
})
*/

/*
// What do promise solve
var storage = {
	get (key, success, error) {
		let value = localStorage.getItem(key)
		if (value === null) {
			error(`${key} could not be found`)
			return
		}

		success(value)
	},
	put (key, value) {
		localStorage.setItem(key, value)
	}
}

storage.get('age', (value) => {
	console.log(value)
}, (error) => {
	console.log(error)
})
*/

/***/ })
/******/ ]);