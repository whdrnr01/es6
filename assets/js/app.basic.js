
/*
// Tagged template literals
var emnum = (strings, ...keys) => {
	keys.forEach((value, index) => {
		if(typeof value === 'number') {
			keys[index] = `<em>${value}</em>`
		}
	})

	var result = strings[0]

	for (let i=0; i < keys.length; i++) {
		result += keys[i] + strings[i + 1]
	}

	return result
}

var name = 'cookie'
var messageCount = 10
var greeting = emnum`Hi ${name}, you have ${messageCount} messages`
console.log(greeting)
*/

/*
// String Letrial
var name = 'cookie'
var greeting = 'Welcome, ' + name
var greeting = `Welcome,  ${name}`
console.log(greeting)

// var link = `<a href="${url}">${title}</a>`

var unreadMessages = 5
var readMessages = 20

var messages = `You have ${unreadMessages + readMessages}`

console.log(messages)

let text = () => {
	return 'Some value'
}
let anchor = `<a href="">${text()}</a>`
console.log(anchor)
*/

/*
// Spread operator
var posts = [
	{title: 'Post One', body: 'Lorem ipsum'},
	{title: 'Post two', body: 'Lorem ipsum dolor'},
]

var newPosts = [
	{title: 'Post three', body: 'Lorem ipsum dolor'},
	{title: 'Post four', body: 'Lorem ipsum dolor'},
]

posts.push(...newPosts)

console.log(posts)
*/

/*
var api = {
	register (payload) {
		console.log(payload)
	}
}

var register = ({email, username, password, first_name}) => {
	api.register({email, username, password, first_name})
}

var component = {
	data: {
		email: 'cookie@sample.com',
		username: 'cookie',
		password: 'password',
		first_name: 'Jeong'
	}
}

register(component.data)
*/


/*
// Shorthand object properties
var sounds = {
	meow() {
		return 'Meow'
	}, 
	hiss() {
		return 'Hiss'
	}	
}

var name = 'Mabel'
var mabel = {name, sounds}

console.log(mabel.sounds.meow())
*/

/*
// function default parameters
var greet = (greeting, name = 'Bob') => {
	return greeting + ', ' + name
}

console.log(greet('Good morning'))
*/

/*
// Arrow functions
// var calc = (one, two) => one + two
// console.log(calc(1, 2))

localStorage.setItem('token', 'ABC')

var storage = {
	get: function (key, callback) {
		callback(localStorage.getItem(key))
	}
}

// storage.get('token', (value) => console.log(value))

var component = {
	data: {
		token: null
	},
	run: function () {
		storage.get('token', (value) => {
			this.data.token = value
		})
	}
}

component.run()

console.log(component)
*/

/*
// Contants
const user = {
	authenticated: true,
	name: 'cookie'
}
user.name = 'Sam'
console.log(user)
*/

/*
// let example 2
var messages = ['Hi there', 'good', 'How are you?']

for (let i=0; i < messages.length; i++) {
	setTimeout(function (){
		console.log(messages[i])
	}, i * (Math.floor(Math.random() * 2500 + 1500)))
}
*/

/*
// let example 1
var results = []

for (let x = 1; x <= 10; x++) {
	// console.log(x)
	results.push(function () {
		return x % 2 === 0 ? 'even' : 'odd'
	})
}

results.forEach(function (result) {
	console.log(result())
})


let name = 'cookie'

{
	let name = 'billy'
}

console.log(name)
*/