
/*
// Axios 
import * as api from './api'

api.getTopic().then((response) => {
	console.log(response.data)
})
*/

/*
// Promise localforage
import localforage from 'localforage'

localforage.config({
	driver: localforage.LOCALSTORAGE,
	storeName: 'Seminary'
})

localforage.getItem('name').then((value) => {
	console.log(value)
})
*/

/*
// Promise localstorage
import storage from './storage'


storage.get('name').then((value) => {
	console.log(value)
})
*/

/*
// Promise Basic
let api = {
	register () {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				if (false) {
					reject({'error': 'Failed'})
				}
				resolve({'name': 'cookie'})
			}, 2000)
		})
	}
}

let component = {
	register () {
		api.register().then((response) => {
			console.log(response)
		}).catch(() => {
			console.log('Registration Failed')
		})
	}
}

component.register()
*/

/*
let p = new Promise((resolve, reject) => {
	var key = 'abc'
	var value = null

	if (value) {
		resolve(value)
	}

	reject(`No key found ${key}`)
})

p.then((value) => {
	console.log(value)
})

p.catch((value) => {
	console.log(value)
})
*/

/*
// What do promise solve
var storage = {
	get (key, success, error) {
		let value = localStorage.getItem(key)
		if (value === null) {
			error(`${key} could not be found`)
			return
		}

		success(value)
	},
	put (key, value) {
		localStorage.setItem(key, value)
	}
}

storage.get('age', (value) => {
	console.log(value)
}, (error) => {
	console.log(error)
})
*/