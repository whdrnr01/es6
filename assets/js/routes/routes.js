var routes = [
	{
		name: 'home',
		path: '/'
	},
	{
		name: 'signup',
		path: '/signup'
	}
]

export { routes }